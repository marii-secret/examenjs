// NO FUI CAPAZ DE HACER EL EJERCICIO, PREFIERO NO COPIAR DE OTRO Y BUENO, DEJÉ LO QUE 
// INTENTÉ HACER PERO ESTOY MUY PERDIDA EN ESTE CONCEPTO DE PROMISES.. PERDON


/** * * * * * * * * * * * * * * * * * * * * * *
*  3. ASINCRONIA                            *
* * * * * * * * * * * * * * * * * * * * * * *

En este ejercicio se comprobará la competencia de los alumnos en el concepto de asincronia 
Se proporcionan 3 archivos  csv separados por comas y se deberán bajar asincronamente (promises) 

A la salida se juntarán los registros de los 3 archivos en un array que será el parametro de entrada 
de la funcion findIPbyName(array, name ,surname) que buscará una entrada en el array y devolverá la IP correspondiente

Una vez hallada la IP ha de mostrarse por pantalla

para llamar a la funcion utilizad el nombre Cari Wederell
*/
// 1 BAJAR ARQUIVOS DESDE InterNET (AXIOS)
// 2 JUNTAR ARCHIVOS EN UN Array
// 3HACER UNA FUNCION CON 3 PARAMETROS (ARRAY, NOMBRE, APELLIDO)
// 4 HACER UN BUCLE PARA BUSCAR DENTRO DE ESTE ARRAY EL CAMPO iP
// 5 LLAMAR LA FUNCION CON EL NOMBRE CITADO Y DEVOLVER IP
// indice 1,2,5



const fsPromises = require('fs');

    const findIPbyName = (array, name ,surname) => {
        const NAME_POSITION = 1;
        const SURNAME_POSITION = 2;
        const IP_POSITION = 5;

        const findedLine = array.find(line => line[NAME_POSITION] == name && line[SURNAME_POSITION] == surname);
          
        return findedLine[IP_POSITION];
         
}

fsPromises.readFile('files /links.csv')
.then((ip) => {
  console.log(ip.toString());
})

Promise.all([mock1,mock2,mock3])
    .then( ()=> {

    })