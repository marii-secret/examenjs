/** * * * * * * * * * * * * * * * * * * * *
*  2. D A D O   E L E C T R o N I C O   *
* * * * * * * * * * * * * * * * * * * * *
 
Simula el uso de un dado electronico cuyos valores al azar irán del 1 al 6. 

- Crea una variable "totalScore" en la que se irá almacenando la puntuación total tras cada una de las tiradas. 
- Una vez alcanzados los 50 puntos el programa se detendrá y se mostrará un mensaje que indique el fin de la partida.
- Debes mostrar por pantalla los distintos valores que nos devuelva el dado (numeros del 1 al 6) así como el valor de 
la variable "totalScore" tras cada una de las tiradas.
*/
totalScore = 0;
lanzarDados = 0;

while(totalScore <= 50){
    tiradaActual = lanzarDados = (Math.floor(1000*(Math.random())%6));
    console.log('lanza dados', tiradaActual);
    totalScore = totalScore + tiradaActual
    console.log('total de puntos',totalScore);
}
console.log('Fin de la partida');
