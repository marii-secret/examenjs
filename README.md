/** * * * * * * * * * * * * * * *
*  1. C A L C U L A D O R A   *
* * * * * * * * * * * * * * * *

Crea un programa que permita realizar sumas, restas, multiplicaciones y divisiones. 

- El programa debe recibir dos numeros (n1, n2).
- Debe existir una variable que permita seleccionar de alguna forma el tipo de operacion (suma, resta, 
multiplicacion o division).
- Opcional: agrega una operacion que permita elevar n1 a la potencia n2.
*/

/** * * * * * * * * * * * * * * * * * * * *
*  2. D A D O   E L E C T R o N I C O   *
* * * * * * * * * * * * * * * * * * * * *
 
Simula el uso de un dado electronico cuyos valores al azar irán del 1 al 6. 

- Crea una variable "totalScore" en la que se irá almacenando la puntuación total tras cada una de las tiradas. 
- Una vez alcanzados los 50 puntos el programa se detendrá y se mostrará un mensaje que indique el fin de la partida.
- Debes mostrar por pantalla los distintos valores que nos devuelva el dado (numeros del 1 al 6) así como el valor de 
la variable "totalScore" tras cada una de las tiradas.
*/

/** * * * * * * * * * * * * * * * * * * * * * *
*  3. ASINCRONIA                            *
* * * * * * * * * * * * * * * * * * * * * * *

En este ejercicio se comprobará la competencia de los alumnos en el concepto de asincronia 
Se proporcionan 3 archivos  csv separados por comas y se deberán bajar asincronamente (promises) 

A la salida se juntarán los registros de los 3 archivos en un array que será el parametro de entrada 
de la funcion findIPbyName(array, name ,surname) que buscará una entrada en el array y devolverá la IP correspondiente

Una vez hallada la IP ha de mostrarse por pantalla

para llamar a la funcion utilizad el nombre Cari Wederell
*/