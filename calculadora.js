/** * * * * * * * * * * * * * * *
*  1. C A L C U L A D O R A   *
* * * * * * * * * * * * * * * *

Crea un programa que permita realizar sumas, restas, multiplicaciones y divisiones. 

- El programa debe recibir dos numeros (n1, n2).
- Debe existir una variable que permita seleccionar de alguna forma el tipo de operacion (suma, resta, 
multiplicacion o division).
- Opcional: agrega una operacion que permita elevar n1 a la potencia n2.
*/
function calculadora(opera,n1,n2){
    n1 = parseFloat(n1);
    n2 = parseFloat(n2);
    switch (opera){
        case 'suma':
            resultado = n1 + n2;
        break;
        case 'resta':
            resultado = n1 - n2;
        break;
        case 'multiplica':
            resultado = n1 * n2;
        break;
        case 'divide':
            resultado = n1 / n2;
        break;
        case 'potencia':
            resultado = Math.pow(n2, n1);
        break;
    }
    return resultado;
}



console.log(calculadora('resta',-100,10))